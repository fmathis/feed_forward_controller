#include "feed_forward_controller/RosJointStateController.h"

namespace feed_forward_controllers
{
    RosJointStateController::RosForwardStateController()
    {}

    RosJointStateController::~RosForwardStateController()
    {
        commandSubscriber.shutdown();
    }

    bool RosJointStateController::init(hardware_interface::RobotHW* robot_hw, ros::NodeHandle& n)
    {
        statePublisher.reset(new realtime_tools::RealtimePublisher<sensor_msgs::JointState>(n, "state", 1));
        commandSubscriber = n.subscribe<sensor_msgs::JointStateCommand>("command", 1, &RosJointStateController::setCommandCB, this);

        jointHandles.clear();
        std::vector<std::string> jointNames;

        if (n.getParam("joints", jointNames))
        {
            if (robot_hw)
            {
                for (unsigned int i = 0; i < jointNames.size(); ++i)
                {
                    jointHandles.push_back(robot_hw->getHandle(jointNames[i]));
                }

                preallocCommandPositions.resize(jointHandles.size(), 0.0);
                preallocCommandVelocities.resize(jointHandles.size(), 0.0);
                preallocCommandEfforts.resize(jointHandles.size(), 0.0);
            }
            else
            {
                ROS_ERROR("EffortJointInterface in RobotHW is empty/null.");
                return false;   //! joints are required
            }
        }
        else
        {
            ROS_ERROR("No joints given (namespace: %s)", n.getNamespace().c_str());
            return false;   //! joints are required
        }

        if (statePublisher && statePublisher->trylock())
        {
            statePublisher->msg_.position.resize(jointHandles.size(), 0.0);
            statePublisher->msg_.velocity.resize(jointHandles.size(), 0.0);
            statePublisher->msg_.effort.resize(jointHandles.size(), 0.0);
            statePublisher->unlock();
        }

        return true;
    }

    void RosJointStateController::starting(const ros::Time& time)
    {
        //! tare commands
        for (unsigned int i = 0; i < jointHandles.size(); ++i)
        {
            preallocCommandPositions[i] = jointHandles[i].getPosition();
            preallocCommandVelocities[i] = jointHandles[i].getVelocity();
            preallocCommandEfforts[i] = jointHandles[i].getEffort();
        }

        bufferCommandPositions.initRT(preallocCommandPositions);
        bufferCommandVelocities.initRT(preallocCommandVelocities);
        bufferCommandEfforts.initRT(preallocCommandEfforts);
    }

    void RosJointStateController::update(const ros::Time& time, const ros::Duration& period)
    {
        preallocCommandPositions = *(bufferCommandPositions.readFromRT());
        preallocCommandVelocities = *(bufferCommandVelocities.readFromRT());
        preallocCommandEfforts = *(bufferCommandEfforts.readFromRT());

        for (unsigned int i = 0; i < jointHandles.size(); ++i)
        {
            jointHandles[i].setCommand(preallocCommandEfforts[i]);
        }

        if (statePublisher && statePublisher->trylock())
        {
            statePublisher->msg_.header.stamp = time;

            for (unsigned int i = 0; i < jointHandles.size(); ++i)
            {
                statePublisher->msg_.position[i] = jointHandles[i].getPosition();
                statePublisher->msg_.velocity[i] = jointHandles[i].getVelocity();
                statePublisher->msg_.effort[i] = jointHandles[i].getEffort();
            }

            statePublisher->unlockAndPublish();
        }
    }

    void RosJointStateController::setCommandCB(const sensor_msgs::JointStatePtr& msg)
    {
        if ((msg->position.size() != jointHandles.size()) ||
                (msg->velocity.size() != jointHandles.size()) ||
                (msg->effort.size() != jointHandles.size()))
        {
            ROS_ERROR_STREAM("Dimension of command (" << msg->position.size() << ", " << msg->velocity.size() << ", " << msg->effort.size() << ") does not match number of joints (" << jointHandles.size() << ")! Not executing!");
            return;
        }

        bufferCommandPositions.writeFromNonRT(msg->position);
        bufferCommandVelocities.writeFromNonRT(msg->velocity);
        bufferCommandEfforts.writeFromNonRT(msg->effort);
    }
}

PLUGINLIB_EXPORT_CLASS(feed_forward_controllers::RosJointStateController, controller_interface::ControllerBase)
