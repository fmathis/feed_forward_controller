#ifndef ROS_JOINT_STATE_CONTROLLER_
#define ROS_JOINT_STATE_CONTROLLER_

#include <ros/node_handle.h>
#include <boost/scoped_ptr.hpp>
#include <hardware_interface/joint_command_interface.h>
#include <hardware_interface/imu_sensor_interface.h>
#include <hardware_interface/force_torque_sensor_interface.h>
#include <realtime_tools/realtime_buffer.h>
#include <realtime_tools/realtime_publisher.h>
#include <pluginlib/class_list_macros.h>
#include "sensor_msgs/JointState.h"

namespace feed_forward_controllers
{

    class RosJointStateController: public controller_interface::Controller<hardware_interface::EffortJointInterface>
    {
    public:
        RosJointStateController();
        virtual ~RosJointStateController();

        bool init(hardware_interface::EffortJointInterface* robot_hw, ros::NodeHandle& n);
        void starting(const ros::Time& time);
        void update(const ros::Time& time, const ros::Duration& period);

        std::vector< hardware_interface::JointHandle > jointHandles;
        realtime_tools::RealtimeBuffer< std::vector<double> > bufferCommandPositions;
        realtime_tools::RealtimeBuffer< std::vector<double> > bufferCommandVelocities;
        realtime_tools::RealtimeBuffer< std::vector<double> > bufferCommandEfforts;
        std::vector<double> preallocCommandPositions;
        std::vector<double> preallocCommandVelocities;
        std::vector<double> preallocCommandEfforts;

    private:
        void setCommandCB(const sensor_msgs::JointStatePtr& msg);

        ros::Subscriber commandSubscriber;
        boost::scoped_ptr< realtime_tools::RealtimePublisher<sensor_msgs::JointState> > statePublisher;
    };
}

#endif
